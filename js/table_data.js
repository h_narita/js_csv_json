$("#table-data-download").on("click", function(){
	$("div#param").empty();
	$("form#table_information").attr("action", "./data/table_data.php");

	$table_header = getTableIndexParam();
  $table_body = getTableDataParam();
  console.log($table_header);
  console.log($table_body);

  // HEADER
	$.each($table_header, function(index){
		var tr_ary = this;
		var tr_ary_index = index;
		$.each(tr_ary, function(index){
			var add_html = '<input type="hidden" name="table_header['+tr_ary_index+']['+index+']" value="'+this+'" >';
			$("div#param").append(add_html);
		});
	});
	// BODY
	$.each($table_body, function(index){
    var tr_ary = this;
		var tr_ary_index = index;
    $.each(tr_ary, function(index){
	    var add_html = '<input type="hidden" name="table_body['+tr_ary_index+']['+index+']" value="'+this+'" >';
      $("div#param").append(add_html);
		});
	});

	$("form#table_information").submit();
	$("div#param").empty();

});


function getTableIndexParam(){
	var table_header = [];
	var th_group = $("table thead tr");
	var rowspan_ary = [];
	$.each(th_group, function(index){
		var $tr = [];
		var colCnt = 0;
		if (rowspan_ary.length > 0){
			forEach(rowspan_ary, function(key, val){
				$tr[key] = val;
			});
		}
		rowspan_ary = [];

		$.each($(this).find("th,td"), function(index){
			var rowspan = $(this).attr("rowspan");
			var colspan = $(this).attr("colspan");

			if(typeof colspan == "undefined" || colspan == "1") {
				$tr.push($(this).text());
				colCnt++;
			} else {
				for(var $i = 0; $i < colspan; $i++){
					$tr.push($(this).text());
					colCnt++;
				}
			}

			if(typeof rowspan !== "undefined" && rowspan !== "1"){
				rowspan_ary[colCnt-1] = $(this).text();
			}
		});
		table_header.push($tr);
	});

	return table_header;
}

function getTableDataParam(){
	var table_body = [];

	var tr_group = $("table tbody tr");
	$.each(tr_group, function(index){
    var $tr = [];
		var $set = $(this).find("th,td");
		var len = $set.length;
		$.each($set, function(index, element){
			$tr.push($(this).text());
		});
		table_body.push($tr);
	});

	if ($("table tfoot tr").length) {
		var tfoot = $("table tfoot tr");
		$.each(tfoot, function(index){
			var $tr = [];
			var $set = $(this).find("th,td");
			var len = $set.length;
			$.each($set, function(index, element){
				$tr.push($(this).text());
			});
			table_body.push($tr);
		});
	}

	return table_body;
}
